# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import models, migrations


class Migration(migrations.Migration):

    dependencies = [
    ]

    operations = [
        migrations.CreateModel(
            name='Battery',
            fields=[
                ('id', models.AutoField(verbose_name='ID', primary_key=True, serialize=False, auto_created=True)),
                ('Name', models.CharField(max_length=20)),
            ],
            options={
            },
            bases=(models.Model,),
        ),
        migrations.CreateModel(
            name='Bluetooth',
            fields=[
                ('id', models.AutoField(verbose_name='ID', primary_key=True, serialize=False, auto_created=True)),
                ('Version', models.FloatField()),
            ],
            options={
            },
            bases=(models.Model,),
        ),
        migrations.CreateModel(
            name='BluetoothType',
            fields=[
                ('id', models.AutoField(verbose_name='ID', primary_key=True, serialize=False, auto_created=True)),
                ('Name', models.CharField(max_length=20)),
            ],
            options={
            },
            bases=(models.Model,),
        ),
        migrations.CreateModel(
            name='CameraFlashType',
            fields=[
                ('id', models.AutoField(verbose_name='ID', primary_key=True, serialize=False, auto_created=True)),
                ('Name', models.CharField(max_length=20)),
            ],
            options={
            },
            bases=(models.Model,),
        ),
        migrations.CreateModel(
            name='CameraStabilization',
            fields=[
                ('id', models.AutoField(verbose_name='ID', primary_key=True, serialize=False, auto_created=True)),
                ('Type', models.CharField(max_length=50)),
            ],
            options={
            },
            bases=(models.Model,),
        ),
        migrations.CreateModel(
            name='Chipset',
            fields=[
                ('id', models.AutoField(verbose_name='ID', primary_key=True, serialize=False, auto_created=True)),
                ('Name', models.CharField(max_length=100)),
            ],
            options={
            },
            bases=(models.Model,),
        ),
        migrations.CreateModel(
            name='Connector',
            fields=[
                ('id', models.AutoField(verbose_name='ID', primary_key=True, serialize=False, auto_created=True)),
                ('Name', models.CharField(max_length=20)),
            ],
            options={
            },
            bases=(models.Model,),
        ),
        migrations.CreateModel(
            name='Core',
            fields=[
                ('id', models.AutoField(verbose_name='ID', primary_key=True, serialize=False, auto_created=True)),
                ('Type', models.CharField(max_length=20)),
            ],
            options={
            },
            bases=(models.Model,),
        ),
        migrations.CreateModel(
            name='ExternalStorageType',
            fields=[
                ('id', models.AutoField(verbose_name='ID', primary_key=True, serialize=False, auto_created=True)),
                ('Name', models.CharField(max_length=100)),
            ],
            options={
            },
            bases=(models.Model,),
        ),
        migrations.CreateModel(
            name='Frequency',
            fields=[
                ('id', models.AutoField(verbose_name='ID', primary_key=True, serialize=False, auto_created=True)),
                ('Value', models.FloatField()),
            ],
            options={
            },
            bases=(models.Model,),
        ),
        migrations.CreateModel(
            name='Gps',
            fields=[
                ('id', models.AutoField(verbose_name='ID', primary_key=True, serialize=False, auto_created=True)),
                ('Name', models.CharField(max_length=20)),
            ],
            options={
            },
            bases=(models.Model,),
        ),
        migrations.CreateModel(
            name='Gpu',
            fields=[
                ('id', models.AutoField(verbose_name='ID', primary_key=True, serialize=False, auto_created=True)),
                ('Name', models.CharField(max_length=250)),
            ],
            options={
            },
            bases=(models.Model,),
        ),
        migrations.CreateModel(
            name='Gsm',
            fields=[
                ('id', models.AutoField(verbose_name='ID', primary_key=True, serialize=False, auto_created=True)),
                ('Value', models.IntegerField(default=0)),
            ],
            options={
            },
            bases=(models.Model,),
        ),
        migrations.CreateModel(
            name='MusicPlayerType',
            fields=[
                ('id', models.AutoField(verbose_name='ID', primary_key=True, serialize=False, auto_created=True)),
                ('Name', models.CharField(max_length=20)),
            ],
            options={
            },
            bases=(models.Model,),
        ),
        migrations.CreateModel(
            name='OperatingSystem',
            fields=[
                ('id', models.AutoField(verbose_name='ID', primary_key=True, serialize=False, auto_created=True)),
                ('Name', models.CharField(max_length=500)),
            ],
            options={
            },
            bases=(models.Model,),
        ),
        migrations.CreateModel(
            name='OperatingSystemVersion',
            fields=[
                ('id', models.AutoField(verbose_name='ID', primary_key=True, serialize=False, auto_created=True)),
                ('Version', models.CharField(max_length=30)),
            ],
            options={
            },
            bases=(models.Model,),
        ),
        migrations.CreateModel(
            name='Protection',
            fields=[
                ('id', models.AutoField(verbose_name='ID', primary_key=True, serialize=False, auto_created=True)),
                ('Name', models.CharField(max_length=50)),
            ],
            options={
            },
            bases=(models.Model,),
        ),
        migrations.CreateModel(
            name='ScreenType',
            fields=[
                ('id', models.AutoField(verbose_name='ID', primary_key=True, serialize=False, auto_created=True)),
                ('Name', models.CharField(max_length=100)),
            ],
            options={
            },
            bases=(models.Model,),
        ),
        migrations.CreateModel(
            name='SimType',
            fields=[
                ('id', models.AutoField(verbose_name='ID', primary_key=True, serialize=False, auto_created=True)),
                ('Name', models.CharField(max_length=30)),
            ],
            options={
            },
            bases=(models.Model,),
        ),
        migrations.CreateModel(
            name='Smartphone',
            fields=[
                ('id', models.AutoField(verbose_name='ID', primary_key=True, serialize=False, auto_created=True)),
                ('Name', models.CharField(max_length=500)),
                ('Photo', models.ImageField(upload_to='')),
                ('Size', models.CharField(max_length=30)),
                ('Weight', models.IntegerField(default=0)),
                ('DoubleSim', models.BooleanField(default=False)),
                ('Ram', models.IntegerField(default=0)),
                ('InternalStorage', models.IntegerField(default=0)),
                ('ExternalStorage', models.IntegerField(default=0)),
                ('Inch', models.FloatField()),
                ('ResolutionHeight', models.IntegerField(default=0)),
                ('ResoutionWidth', models.FloatField()),
                ('Dpi', models.IntegerField(default=0)),
                ('MultiTouch', models.BooleanField(default=False)),
                ('Colors', models.IntegerField(default=0)),
                ('Megapixels', models.IntegerField(default=0)),
                ('CameraResolutionHeight', models.IntegerField(default=0)),
                ('CameraResoutionWidth', models.FloatField()),
                ('CameraAutoFocus', models.BooleanField(default=False)),
                ('TouchFocus', models.BooleanField(default=False)),
                ('CameraFlash', models.BooleanField(default=False)),
                ('Hdr', models.BooleanField(default=False)),
                ('GeoTagging', models.BooleanField(default=False)),
                ('FaceDetection', models.BooleanField(default=False)),
                ('SmileDetection', models.BooleanField(default=False)),
                ('FrontCameraMegapixels', models.IntegerField(default=0)),
                ('Panoramic', models.BooleanField(default=False)),
                ('ManualIso', models.BooleanField(default=False)),
                ('Fps', models.IntegerField(default=0)),
                ('VideoAutoFocus', models.BooleanField(default=False)),
                ('VideoStabilization', models.BooleanField(default=False)),
                ('SlowMotion', models.IntegerField(default=0)),
                ('DualRec', models.BooleanField(default=False)),
                ('StereoSoundRec', models.BooleanField(default=False)),
                ('PhotoInVideo', models.BooleanField(default=False)),
                ('VideoFrontCamera', models.IntegerField(default=0)),
                ('Irda', models.BooleanField(default=False)),
                ('Nfc', models.BooleanField(default=False)),
                ('Gprs', models.BooleanField(default=False)),
                ('Edge', models.BooleanField(default=False)),
                ('Umts', models.BooleanField(default=False)),
                ('Hsdpa', models.BooleanField(default=False)),
                ('Hsupa', models.BooleanField(default=False)),
                ('HspaPlus', models.BooleanField(default=False)),
                ('Lte', models.BooleanField(default=False)),
                ('MaxDownload', models.FloatField()),
                ('MinDownload', models.FloatField()),
                ('Accelerometer', models.BooleanField(default=False)),
                ('Proximity', models.BooleanField(default=False)),
                ('Periscope', models.BooleanField(default=False)),
                ('Compass', models.BooleanField(default=False)),
                ('Barometer', models.BooleanField(default=False)),
                ('MicrophoneReduction', models.BooleanField(default=False)),
                ('Radio', models.BooleanField(default=False)),
                ('Tv', models.BooleanField(default=False)),
                ('Vibration', models.BooleanField(default=False)),
                ('VoiceOver', models.BooleanField(default=False)),
                ('Other', models.CharField(max_length=200)),
                ('BatteryTalkLife', models.IntegerField(default=0)),
                ('BatteryStanbyLife', models.IntegerField(default=0)),
                ('BatteryAmpere', models.IntegerField(default=0)),
                ('BatteryRemovable', models.BooleanField(default=False)),
                ('Battery', models.ForeignKey(to='smartphone.Battery')),
                ('Bluetooth', models.ForeignKey(to='smartphone.Bluetooth')),
                ('BluetoothType', models.ManyToManyField(to='smartphone.BluetoothType')),
                ('CameraFlashType', models.ForeignKey(to='smartphone.CameraFlashType')),
                ('CameraStabilization', models.ForeignKey(to='smartphone.CameraStabilization')),
                ('Chipset', models.ForeignKey(to='smartphone.Chipset')),
                ('Connector', models.ForeignKey(to='smartphone.Connector')),
                ('Core', models.ForeignKey(to='smartphone.Core')),
                ('ExternalStorageType', models.ForeignKey(to='smartphone.ExternalStorageType')),
                ('Frequency', models.ForeignKey(to='smartphone.Frequency')),
                ('Gps', models.ForeignKey(to='smartphone.Gps')),
                ('Gpu', models.ForeignKey(to='smartphone.Gpu')),
                ('Gsm', models.ManyToManyField(to='smartphone.Gsm')),
                ('MusicPlayerType', models.ManyToManyField(to='smartphone.MusicPlayerType')),
                ('OperatingSystem', models.ForeignKey(to='smartphone.OperatingSystem')),
                ('OperatingSystemVersion', models.ForeignKey(to='smartphone.OperatingSystemVersion')),
                ('Protection', models.ForeignKey(to='smartphone.Protection')),
                ('ScreenType', models.ForeignKey(to='smartphone.ScreenType')),
                ('Sim', models.ForeignKey(to='smartphone.SimType')),
            ],
            options={
            },
            bases=(models.Model,),
        ),
        migrations.CreateModel(
            name='TouchScreen',
            fields=[
                ('id', models.AutoField(verbose_name='ID', primary_key=True, serialize=False, auto_created=True)),
                ('Type', models.CharField(max_length=50)),
            ],
            options={
            },
            bases=(models.Model,),
        ),
        migrations.CreateModel(
            name='TvExit',
            fields=[
                ('id', models.AutoField(verbose_name='ID', primary_key=True, serialize=False, auto_created=True)),
                ('Name', models.CharField(max_length=20)),
            ],
            options={
            },
            bases=(models.Model,),
        ),
        migrations.CreateModel(
            name='VideoFrontCameraOption',
            fields=[
                ('id', models.AutoField(verbose_name='ID', primary_key=True, serialize=False, auto_created=True)),
                ('Name', models.CharField(max_length=50)),
            ],
            options={
            },
            bases=(models.Model,),
        ),
        migrations.CreateModel(
            name='VideoPlayerType',
            fields=[
                ('id', models.AutoField(verbose_name='ID', primary_key=True, serialize=False, auto_created=True)),
                ('Name', models.CharField(max_length=20)),
            ],
            options={
            },
            bases=(models.Model,),
        ),
        migrations.CreateModel(
            name='VideoQuality',
            fields=[
                ('id', models.AutoField(verbose_name='ID', primary_key=True, serialize=False, auto_created=True)),
                ('Name', models.CharField(max_length=20)),
            ],
            options={
            },
            bases=(models.Model,),
        ),
        migrations.CreateModel(
            name='Wifi',
            fields=[
                ('id', models.AutoField(verbose_name='ID', primary_key=True, serialize=False, auto_created=True)),
                ('Version', models.CharField(max_length=20)),
            ],
            options={
            },
            bases=(models.Model,),
        ),
        migrations.CreateModel(
            name='WifiType',
            fields=[
                ('id', models.AutoField(verbose_name='ID', primary_key=True, serialize=False, auto_created=True)),
                ('Name', models.CharField(max_length=20)),
            ],
            options={
            },
            bases=(models.Model,),
        ),
        migrations.AddField(
            model_name='smartphone',
            name='TouchScreen',
            field=models.ForeignKey(to='smartphone.TouchScreen'),
            preserve_default=True,
        ),
        migrations.AddField(
            model_name='smartphone',
            name='TvExit',
            field=models.ManyToManyField(to='smartphone.TvExit'),
            preserve_default=True,
        ),
        migrations.AddField(
            model_name='smartphone',
            name='VideoFrontCameraOption',
            field=models.ForeignKey(to='smartphone.VideoFrontCameraOption'),
            preserve_default=True,
        ),
        migrations.AddField(
            model_name='smartphone',
            name='VideoPlayerType',
            field=models.ManyToManyField(to='smartphone.VideoPlayerType'),
            preserve_default=True,
        ),
        migrations.AddField(
            model_name='smartphone',
            name='VideoQuality',
            field=models.ForeignKey(to='smartphone.VideoQuality'),
            preserve_default=True,
        ),
        migrations.AddField(
            model_name='smartphone',
            name='Wifi',
            field=models.ForeignKey(to='smartphone.Wifi'),
            preserve_default=True,
        ),
        migrations.AddField(
            model_name='smartphone',
            name='WifiType',
            field=models.ManyToManyField(to='smartphone.WifiType'),
            preserve_default=True,
        ),
    ]
