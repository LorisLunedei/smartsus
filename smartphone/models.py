﻿from django.db import models

# Create your models here.
class OperatingSystemVersion(models.Model):
    Version = models.CharField(max_length=30)
    def __str__(self):              # __unicode__ on Python 2
        return self.Version

class OperatingSystem(models.Model):
    Name = models.CharField(max_length=500)
    def __str__(self):
        return self.Name

class Gsm(models.Model):
    Value = models.IntegerField(default=0)
    def __str__(self):
        return str(self.Value)

class SimType(models.Model):
    Name = models.CharField(max_length=30)
    def __str__(self):
        return self.Name

class Frequency(models.Model):
    Value = models.FloatField()
    def __str__(self):
        return str(self.Value)

class Core(models.Model):
    Type = models.CharField(max_length=20)
    def __str__(self):
        return str(self.Value)

class Chipset(models.Model):
    Name = models.CharField(max_length=100)
    def __str__(self):
        return self.Name

class Gpu(models.Model):
    Name = models.CharField(max_length=250)
    def __str__(self):
        return self.Name

class ExternalStorageType(models.Model):
    Name = models.CharField(max_length=100)
    def __str__(self):
        return self.Name

class ScreenType(models.Model):
    Name = models.CharField(max_length=100)
    def __str__(self):
        return self.Name

class TouchScreen(models.Model):
    Type = models.CharField(max_length=50)
    def __str__(self):
        return self.Type

class Protection(models.Model):
    Name = models.CharField(max_length=50)
    def __str__(self):
        return self.Name

class CameraStabilization(models.Model):
    Type = models.CharField(max_length=50)
    def __str__(self):
        return self.Type

class CameraFlashType(models.Model):
    Name = models.CharField(max_length=20)
    def __str__(self):
        return self.Name

class VideoQuality(models.Model):
    Name = models.CharField(max_length=20)
    def __str__(self):
        return self.Name

class VideoFrontCameraOption(models.Model):
    Name = models.CharField(max_length=50)
    def __str__(self):
        return self.Name

class Bluetooth(models.Model):
    Version = models.FloatField()
    def __str__(self):
        return str(self.Version)

class BluetoothType(models.Model):
    Name = models.CharField(max_length=20)
    def __str__(self):
        return str(self.Name)

class Wifi(models.Model):
    Version = models.CharField(max_length=20)
    def __str__(self):
        return str(self.Version)

class WifiType(models.Model):
    Name = models.CharField(max_length=20)
    def __str__(self):
        return str(self.Name)

class Connector(models.Model):
    Name = models.CharField(max_length=20)
    def __str__(self):
        return str(self.Name)

class Gps(models.Model):
    Name = models.CharField(max_length=20)
    def __str__(self):
        return str(self.Name)

class MusicPlayerType(models.Model):
    Name = models.CharField(max_length=20)
    def __str__(self):
        return str(self.Name)

class VideoPlayerType(models.Model):
    Name = models.CharField(max_length=20)
    def __str__(self):
        return str(self.Name)

class TvExit(models.Model):
    Name = models.CharField(max_length=20)
    def __str__(self):
        return str(self.Name)

class Battery(models.Model):
    Name = models.CharField(max_length=20)
    def __str__(self):
        return str(self.Name)

class Smartphone(models.Model):
    Name = models.CharField(max_length=500)
    Photo = models.ImageField()
    Size = models.CharField(max_length=30)
    Weight = models.IntegerField(default=0)
    OperatingSystem = models.ForeignKey(OperatingSystem)
    OperatingSystemVersion = models.ForeignKey(OperatingSystemVersion) #Condizionale rispetto a Operating System
    Gsm = models.ManyToManyField(Gsm)
    DoubleSim = models.BooleanField(default=False)
    Sim = models.ForeignKey(SimType)
    Chipset = models.ForeignKey(Chipset)
    Frequency = models.ForeignKey(Frequency)
    Core = models.ForeignKey(Core)
    Bit = (("32 Bit"),
        ("64 Bit"))
    Gpu = models.ForeignKey(Gpu)
    Ram = models.IntegerField(default=0)
    InternalStorage = models.IntegerField(default=0)
    ExternalStorage = models.IntegerField(default=0)
    ExternalStorageType = models.ForeignKey(ExternalStorageType)
    Inch = models.FloatField()
    ResolutionHeight = models.IntegerField(default=0)
    ResoutionWidth = models.FloatField()
    Dpi = models.IntegerField(default=0)
    ScreenType = models.ForeignKey(ScreenType)
    TouchScreen = models.ForeignKey(TouchScreen)
    MultiTouch = models.BooleanField(default=False)
    Colors = models.IntegerField(default=0) #unità di misura in milioni
    Protection = models.ForeignKey(Protection)
    Megapixels = models.IntegerField(default=0)
    CameraResolutionHeight = models.IntegerField(default=0)
    CameraResoutionWidth = models.FloatField()
    CameraStabilization = models.ForeignKey(CameraStabilization)
    CameraAutoFocus = models.BooleanField(default=False)
    TouchFocus = models.BooleanField(default=False)
    CameraFlash = models.BooleanField(default=False)
    CameraFlashType = models.ForeignKey(CameraFlashType) #Da valutare se è campo condizionale rispetto a Flash
    Hdr = models.BooleanField(default=False)
    GeoTagging = models.BooleanField(default=False)
    FaceDetection = models.BooleanField(default=False)
    SmileDetection = models.BooleanField(default=False)
    FrontCameraMegapixels = models.IntegerField(default=0)
    Panoramic = models.BooleanField(default=False)
    ManualIso = models.BooleanField(default=False)
    VideoQuality = models.ForeignKey(VideoQuality)
    Fps = models.IntegerField(default=0)
    VideoAutoFocus = models.BooleanField(default=False)
    VideoStabilization = models.BooleanField(default=False)
    SlowMotion = models.IntegerField(default=0)
    DualRec = models.BooleanField(default=False)
    StereoSoundRec = models.BooleanField(default=False)
    PhotoInVideo = models.BooleanField(default=False)
    VideoFrontCamera = models.IntegerField(default=0)
    #VideoFrontCameraQuality = models.ForeignObject(VideoQuality)
    VideoFrontCameraOption = models.ForeignKey(VideoFrontCameraOption)
    Bluetooth = models.ForeignKey(Bluetooth)
    BluetoothType = models.ManyToManyField(BluetoothType)
    Wifi = models.ForeignKey(Wifi)
    WifiType = models.ManyToManyField(WifiType)
    Irda = models.BooleanField(default=False)
    Connector = models.ForeignKey(Connector)
    Nfc = models.BooleanField(default=False)
    Gps = models.ForeignKey(Gps)
    Gprs = models.BooleanField(default=False)
    Edge = models.BooleanField(default=False)
    Umts = models.BooleanField(default=False)
    Hsdpa = models.BooleanField(default=False)
    Hsupa = models.BooleanField(default=False)
    HspaPlus = models.BooleanField(default=False)
    Lte = models.BooleanField(default=False)
    MaxDownload = models.FloatField()
    MinDownload = models.FloatField()
    Accelerometer = models.BooleanField(default=False)
    Proximity = models.BooleanField(default=False)
    Periscope = models.BooleanField(default=False)
    Compass = models.BooleanField(default=False)
    Barometer = models.BooleanField(default=False)
    MicrophoneReduction = models.BooleanField(default=False)
    MusicPlayerType = models.ManyToManyField(MusicPlayerType)
    VideoPlayerType = models.ManyToManyField(VideoPlayerType)
    Radio = models.BooleanField(default=False)
    Tv = models.BooleanField(default=False)
    TvExit = models.ManyToManyField(TvExit)
    Vibration = models.BooleanField(default=False)
    VoiceOver = models.BooleanField(default=False)
    Other = models.CharField(max_length=200)
    Battery = models.ForeignKey(Battery)
    BatteryTalkLife = models.IntegerField(default=0)
    BatteryStanbyLife = models.IntegerField(default=0)
    BatteryAmpere = models.IntegerField(default=0)
    BatteryRemovable = models.BooleanField(default=False)
    def __str__(self):
         return self.Name
