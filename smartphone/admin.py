﻿from django.contrib import admin
from smartphone.models import OperatingSystemVersion
from smartphone.models import OperatingSystem
from smartphone.models import Gsm
from smartphone.models import SimType
from smartphone.models import Frequency
from smartphone.models import Core
from smartphone.models import Chipset
from smartphone.models import Gpu
from smartphone.models import ExternalStorageType
from smartphone.models import ScreenType
from smartphone.models import TouchScreen
from smartphone.models import Protection
from smartphone.models import CameraStabilization
from smartphone.models import CameraFlashType
from smartphone.models import VideoQuality
from smartphone.models import VideoFrontCameraOption
from smartphone.models import Bluetooth
from smartphone.models import BluetoothType
from smartphone.models import Wifi
from smartphone.models import WifiType
from smartphone.models import Connector
from smartphone.models import Gps
from smartphone.models import MusicPlayerType
from smartphone.models import VideoPlayerType
from smartphone.models import TvExit
from smartphone.models import Battery
from smartphone.models import Smartphone

# Register your models here.
admin.site.register(OperatingSystemVersion)
admin.site.register(OperatingSystem)
admin.site.register(Gsm)
admin.site.register(SimType)
admin.site.register(Frequency)
admin.site.register(Core)
admin.site.register(Chipset)
admin.site.register(Gpu)
admin.site.register(ExternalStorageType)
admin.site.register(ScreenType)
admin.site.register(TouchScreen)
admin.site.register(Protection)
admin.site.register(CameraStabilization)
admin.site.register(CameraFlashType)
admin.site.register(VideoQuality)
admin.site.register(VideoFrontCameraOption)
admin.site.register(Bluetooth)
admin.site.register(BluetoothType)
admin.site.register(Wifi)
admin.site.register(WifiType)
admin.site.register(Connector)
admin.site.register(Gps)
admin.site.register(MusicPlayerType)
admin.site.register(VideoPlayerType)
admin.site.register(TvExit)
admin.site.register(Battery)
admin.site.register(Smartphone)


